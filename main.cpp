#include <sys/uio.h>
#include "process.h"
#include <iostream>
#include <unistd.h>
#include "addresses.h"

using namespace addresses;

int main() {
	std::cout << "..', ............" << std::endl;
    std::cout << ".;co:'.....''..'." << std::endl;
    std::cout << "c:cox:;doxkolc:::" << std::endl;
    std::cout << "lkl:c'':xKKl:cc::" << std::endl;
    std::cout << "Xkc....'XMMWOc;::" << std::endl;
    std::cout << "Ood:'..,XWMWdkl,;" << std::endl;
    std::cout << "N0lkOkdxWMMNkdXkd" << std::endl;
    std::cout << "WN0;lxKNNWNkcoK0c" << std::endl;
    std::cout << "WWN;   .,;...;:';" << std::endl;
    std::cout << "NNX.   ;.  . ..'d" << std::endl;
		
	process* k = new process("warsow.x86_64");
	int localPlayer;
	k->read(k->baseAddress + localPlayerPointerOffset, &localPlayer, sizeof(int));
	float healthy = 10000;
	while(true)  {
		k->write(localPlayer + healthOffset, &healthy, sizeof(float));
		usleep(10000);
	}
}
