#include "process.h" 


process::process(std::string name) {
	pid = getPid(name);
	if(pid != 0) {
		std::cout << "Process found. PID: " << pid << std::endl;
	} else {
		std::cout << "Is Warsow running?" << std::endl;
		exit(EXIT_SUCCESS);
	}
	baseAddress = getBaseAddress();
}

bool process::numsOnly(char* str) {
	char nums[10] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
	bool valid = true;
	for(int i = 0; i < strlen(str); i++) {
		bool found = false;
		for(int j = 0; j < 10; j++)	 {
			if(str[i] == nums[j]) {
				found = true;
			}
		}
		if(found == false) {
			valid = false;
			break;
		}
	}
	return valid;
}

int process::getPid(std::string name) {
	DIR* procdir;	
	dirent* curr;
	procdir = opendir("/proc/");
	while(curr = readdir(procdir)) {
		if(numsOnly(curr->d_name)) {
			std::ifstream file;
			std::string filename;
			char procname[256];
			filename = "/proc/" + (std::string)curr->d_name + "/comm";
			file.open(filename);
			file >> procname;		
			if(procname == name) {
				closedir(procdir);
				return atoi(curr->d_name);
			}
		}
	}
	return 0;
}

bool process::read(uintptr_t address, void* buf, unsigned int size) {
	struct iovec local[1];
	struct iovec remote[1];
	
	local[0].iov_base = buf;
	local[0].iov_len = size;	
	
	remote[0].iov_base = (void*) address;
	remote[0].iov_len = size;

	ssize_t read = process_vm_readv(pid, local, 1, remote, 1, 0);

	if(read == size) {
		return true;
	} else if (read == -1) {
		if(errno == 3) {  //ESRCH: no such process
			std::cout << "Game closed. Exiting." << std::endl;
			exit(EXIT_SUCCESS);
		}
		return false;
	} else {
		return false;
	}
}

bool process::write(uintptr_t address, void* buf, unsigned int size) {
	struct iovec local[1];
	struct iovec remote[1];

	local[0].iov_base = buf;
	local[0].iov_len = size;	
	
	remote[0].iov_base = (void*) address;
	remote[0].iov_len = size;

	ssize_t written = process_vm_writev(pid, local, 1, remote, 1, 0);

	if(written == size) {
		return true;
	} else if (written == -1) {
		if(errno == 3) {  //ESRCH: no such process
			std::cout << "Game closed. Exiting." << std::endl;
			exit(EXIT_SUCCESS);
		}
		return false;
	} else {
		return false;
	}
}

uintptr_t process::getBaseAddress() {
	std::ifstream mapFile("/proc/" + std::to_string(pid) + "/maps");
	std::string range;
	std::string baseAddress;
	mapFile >> range;
	baseAddress = range.substr(0, range.find("-"));
	return (uintptr_t) std::stoul(baseAddress, NULL, 16);
}
