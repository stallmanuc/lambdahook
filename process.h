#include <string>
#include <dirent.h>
#include <strings.h>
#include <string.h>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <unistd.h>
#include <cerrno>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <limits.h>
#include <sys/uio.h>



class process {
private:
	int pid; 
	
	int getPid(std::string name);
	
	uintptr_t getBaseAddress();

	bool numsOnly(char* str);
public:
	uintptr_t baseAddress;

	process(std::string name);
	~process();


	bool write(uintptr_t address, void* buf, unsigned int size);
	bool read(uintptr_t address, void* buf, unsigned int size);
};

